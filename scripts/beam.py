def calc_beam(name):
    # the beam number can be found by parsing the number in the string pattern "_B012."
    beam = 0
    try:
        position = name.find("_B")
        if position>=0:
           beam_string = name[position:position+6]
           if beam_string.find(".") == 5:
               beam = int(beam_string[2:5])

           return beam
    except:
        pass

    return 0