create_database = """
CREATE DATABASE adex_cache
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
"""

drop_table_skyviews = """
   DROP TABLE IF EXISTS public.skyviews;
"""

create_table_skyviews = """
CREATE TABLE public.skyviews
(
    "id" SERIAL,
    "pid" character varying(50),
    "name" character varying(50),
    "observation" character varying(50),
    "beam" integer,
    "ra" double precision,
    "dec" double precision,
    "collection" character varying(50),
    "level" integer,
    "dataproduct_type" character varying(50),
    "dataproduct_subtype" character varying(50),
    "access_url" character varying(255),
    CONSTRAINT skyviews_pkey PRIMARY KEY (id)
);
"""


insert_into_skyviews = """
INSERT INTO public.skyviews
(
    name,
    pid,
    observation,
    beam,
    ra,
    dec,
    collection,
    level,
    dataproduct_type,
    dataproduct_subtype,
    access_url) 
    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
"""

insert_into_skyviews_django = """
INSERT INTO adex_cache_skyview
(
    name,
    pid,
    observation,
    beam,
    ra,
    dec,
    collection,
    level,
    dataproduct_type,
    dataproduct_subtype,
    access_url) 
    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
"""

select_from_alta = """
SELECT 
"name" as name,
"PID" as pid,
"storageRef" as access_url, 
"RA" as ra, dec, 
"dataProductType" as dt, 
"dataProductSubType" as dst, 
"datasetID" as observation 

FROM api_dataproduct as table1
INNER JOIN api_dataentity as table2
INNER JOIN api_entity as table3
ON table3.altaobject_ptr_id = table2.entity_ptr_id
ON table1.dataentity_ptr_id = table2.entity_ptr_id;
"""


insert_into_primarydp = """
INSERT INTO adex_cache_primarydp
(
    pid,
    name,
    dp_type,
    format,
    locality,
    access_url,
    ra,
    dec,
    equinox,
    exposure_time,
    central_frequency,
    frequency_resolution,
    time_resolution,
    bandwidth,
    release_date,
    data_provider,
    PSF_size,
    sky_footprint,
    dataset_id,
    parent
    ) 
    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
"""