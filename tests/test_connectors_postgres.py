import types
import unittest
import argparse
from scraper.postgres.connectors.apertif import Inspectionplots


class TestConnectorPostgresApertif(unittest.TestCase):
    def test_translate(self):
        # arrange
        connector = Inspectionplots()
        name = 'my_name'
        pid = 5
        url = 'some_access_url'
        dataset_id = 23
        row = (name, pid, url, '', '', dataset_id)

        args = argparse.Namespace(collection='my_collection')

        expected = {
            'pid': pid,
            'name': name,
            'dp_type': 'diagnostic-plots',
            'format': 'png',
            'locality': 'online',
            'access_url': url,
            'dataset_id': str(dataset_id),
            'collection': 'my_collection',
        }

        # act
        actual = connector.translate(row=row, args=args)

        # assert
        self.assertDictEqual(actual, expected)
