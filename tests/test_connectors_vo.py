import argparse
import unittest

from scraper.vo.connectors.Apertif_DR1 import ContinuumImagesConnector
from scraper.vo.connectors.LotssDR2 import LotssDR2Mosaics
from scraper.vo.connectors.Linc import LincSkymap, LincCalibratedVisibilities, LincSecondary
from scraper.vo.connectors.ALMA import Obscore as ALMA_Obscore

class TestConnectorVOApertif(unittest.TestCase):

    def test_continuum_images_translate(self):

        # arrange
        row = {
            'accref': 'https://vo.astron.nl/getproduct/APERTIF_DR1/190807041_AP_B001/image_mf_02.fits',
            'freqmin' : 1360.36,
            'freqmax' : 1360.36,
            'imagetitle': '190807041_AP_B001',
            'pub_did': 'ivo://astron.nl/~?APERTIF_DR1/190807041_AP_B001/image_mf_02.fits',
            'centeralpha': 208.360378733881,
            'centerdelta': 52.3613884370237,
            'wcs_equinox': '2000.0',
            'obsid': '190807041',
            'coverage' : None,
        }
        args = argparse.Namespace(collection='apertif-dr1')
        expected = {'pid': 'ivo://astron.nl/~?APERTIF_DR1/190807041_AP_B001/image_mf_02.fits',
                    'name': '190807041_AP_B001',
                    'dp_type': 'science_skymap',
                    'format': 'fits',
                    'locality': 'online',
                    'access_url': 'https://vo.astron.nl/getproduct/APERTIF_DR1/190807041_AP_B001/image_mf_02.fits',
                    'ra': 208.360378733881,
                    'dec': 52.3613884370237,
                    'equinox': '2000.0',
                    'exposure_time': None,
                    'central_frequency': 1360.3599853515625,
                    'frequency_resolution': None,
                    'time_resolution': None,
                    'bandwidth': None,
                    'release_date': '2023-03-20T12:34:21.433848',
                    'data_provider': 'ASTRON',
                    'PSF_size': None,
                    'sky_footprint': None,
                    'dataset_id': '190807041',
                    'collection': 'apertif-dr1',
                    'pipeline_url' : 'apercal_pipeline',
                    'pipeline_version': '2.5'}
        expected = expected

        # act
        connector = ContinuumImagesConnector()
        actual = connector.translate(row=row, args=args)

        # assert contents ignoring order
        self.assertCountEqual(actual, expected)


class TestConnectorVOLinc(unittest.TestCase):

    def test_linc_calibrated_visibilities_translate(self):
        # arrange
        row = {
            'id' : 'L86296_SAP001_SB239_uv.MS_bd9b6f14.tar',
            'access_url': 'srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/commissioning2013/86296/L86296_SAP001_SB239_uv.MS_bd9b6f14.tar',
            'access_format': 'archive/tar',
            'ra': 202.784583333333,
            'dec': 30.5091666666667,
            'freq_bandwidth' : 0.0030517578,
            'dynamic_range' : 8.0,
            'f_min': 164.55078125,
            'f_max': 164.74609375,
            't_start': 58801.74865393527,
            't_end': 58801.76342592575,
            'equinox': 'J2000',
            't_exptime' : 28800.0,
            'mime': 'image/fits',
            'activity': '86796',
            'pipeline_access_url': '',
            'pipeline_version': 'n/a',
            's_region' : None
        }
        args = argparse.Namespace(collection='linc_skymap')
        expected = {'pid': 'L86296_SAP001_SB239_uv.MS_bd9b6f14.tar',
                    'name': 'L86296_SAP001_SB239_uv.MS_bd9b6f14.tar',
                    'dp_type': 'die-calibrated-visibilities',
                    'format': 'archive/tar',
                    'locality': 'tape',
                    'access_url': 'srm://lofar-srm.fz-juelich.de:8443/pnfs/fz-juelich.de/data/lofar/ops/projects/commissioning2013/86296/L86296_SAP001_SB239_uv.MS_bd9b6f14.tar',
                    'ra': 202.784583333333,
                    'dec': 30.5091666666667,
                    'equinox': 'J2000',
                    'exposure_time': '28800.0',
                    'central_frequency': 164.6484375,
                    'frequency_resolution': None,
                    'time_resolution': None,
                    'bandwidth': 0.0030517578125,
                    'release_date': None,
                    'data_provider': 'ASTRON',
                    'PSF_size': None,
                    'sky_footprint': None,
                    'dataset_id': '86796',
                    'collection': 'linc_visibilities',
                    'pipeline_url': '',
                    'pipeline_version': 'n/a'}

        # act
        connector = LincCalibratedVisibilities()
        actual = connector.translate(row=row, args=args)

        # assert contents ignoring order
        self.assertCountEqual(actual, expected)

    def test_linc_skymap_translate(self):

        # arrange
        row = {
            'accref': 'http://145.38.188.72/getproduct/focusProject/test/APERTIF_DR1-191114041_AP_B012-image_mf_02.fits',
            'imageTitle': 'APERTIF_DR1-191114041_AP_B012-image_mf_02',
            'centerAlpha': 334.8973313908905,
            'centerDelta': 43.06170943588219,
            'f_min': 1360124611.85,
            'f_max': 1360905861.85,
            't_start' : 58801.74865393527,
            't_end' : 58801.76342592575,
            'wcs_equinox': '2000.0',
            'mime' : 'image/fits',
            'activity' : '86794',
            'pipeline_access_url' : 'https://somewhereovertherainbow.com/repo/pipeline',
            'pipeline_version' : 'v-1.03'
        }
        args = argparse.Namespace(collection='linc_skymap')
        expected = {'pid': 'APERTIF_DR1-191114041_AP_B012-image_mf_02',
                    'name': 'APERTIF_DR1-191114041_AP_B012-image_mf_02',
                    'dp_type': 'qa-skymap',
                    'format': 'image/fits',
                    'locality': 'online',
                    'access_url': 'http://145.38.188.72/getproduct/focusProject/test/APERTIF_DR1-191114041_AP_B012-image_mf_02.fits',
                    'ra': 334.8973313908905,
                    'dec': 43.06170943588219,
                    'equinox': '2000.0',
                    'exposure_time': 0.014771990478038788,
                    'central_frequency': 1360.5152368499998,
                    'frequency_resolution': None,
                    'time_resolution': None,
                    'bandwidth': None,
                    'release_date': None,
                    'data_provider': 'ASTRON',
                    'PSF_size': None,
                    'sky_footprint': None,
                    'dataset_id': '86794',
                    'collection': 'linc_skymap',
                    'pipeline_url' : 'https://somewhereovertherainbow.com/repo/pipeline',
                    'pipeline_version': 'v-1.03'}

        # act
        connector = LincSkymap()
        actual = connector.translate(row=row, args=args)

        # assert contents ignoring order
        self.assertCountEqual(actual, expected)

    def test_linc_secondary_translate(self):
        # arrange
        row = {
            'access_url': 'https://somewhereovertherainbow/inspection-plots/86794-uv-coverage.png',
            'access_format': 'image/png',
            'activity': '86794',
            'type': 'inspection-plot',
        }
        args = argparse.Namespace(collection='linc_skymap')
        expected = {'pid': 'https://somewhereovertherainbow/inspection-plots/86794-uv-coverage.png',
                    'name': 'https://somewhereovertherainbow/inspection-plots/86794-uv-coverage.png',
                    'dp_type': 'diagnostic-plot',
                    'format': 'other',
                    'locality': 'online',
                    'access_url': 'https://somewhereovertherainbow/inspection-plots/86794-uv-coverage.png',
                    'dataset_id': '86794',
                    'collection': 'linc_ancillary'}

        # act
        connector = LincSecondary()
        actual = connector.translate(row=row, args=args)

        # assert contents ignoring order
        self.assertCountEqual(actual, expected)


class TestConnectorVOLotssDR2(unittest.TestCase):
    def test_mosaics_translate(self):
        # arrange
        row = {
            'accref': 'https://vo.astron.nl/getproduct/APERTIF_DR1/190807041_AP_B001/image_mf_02.fits',
            'freqmin': 1360.36,
            'freqmax': 1360.36,
            'imagetitle': '190807041_AP_B001',
            'data_pid': '21.12136/ede3eff0-266a-465a-af68-6f292391485f',
            'centeralpha': 208.360378733881,
            'centerdelta': 52.3613884370237,
            'wcs_equinox': '2000.0',
            'lofar_obsids': [689778],
        }
        args = argparse.Namespace(collection='lotts-dr2')
        expected = {'pid': '21.12136/ede3eff0-266a-465a-af68-6f292391485f',
                    'name': 'P000+23_mosaic-blanked.fits',
                    'dp_type': 'unknown',
                    'format': 'other',
                    'locality': 'online',
                    'access_url': 'https://vo.astron.nl/getproduct/LoTSS-DR2/P000%2B23',
                    'ra': 0.03125,
                    'dec': 23.3953,
                    'equinox': '2000.0',
                    'exposure_time': None,
                    'central_frequency': None,
                    'frequency_resolution': None,
                    'time_resolution': None,
                    'bandwidth': None,
                    'release_date': '2023-03-20T14:01:36.328040',
                    'data_provider': 'ASTRON',
                    'PSF_size': None,
                    'sky_footprint': None,
                    'dataset_id': '689778',
                    'activity': None,
                    'collection': 'lotts-dr2'}

        # act
        connector = LotssDR2Mosaics()
        actual = connector.translate(row=row, args=args)

        # assert contents ignoring order
        self.assertCountEqual(actual, expected)

class TestConnectorALMAObscore(unittest.TestCase):
    def test_obscore_translate(self):
        # arrange
        row = {
            'access_url': 'http://jvo.nao.ac.jp/skynode/do/download/alma/public/siav2/ALMA01059076_00_00_00',
            'target_name': 'emb_10',
            'dataproduct_type' : 'IMAGE',
            'data_id': 'ALMA01059076_00_00_00',
            's_ra': 248.00413143,
            's_dec': -24.9451725,
            'obs_release_date' : "2017-11-27",
            's_region' : "POLYGON(248.0085,-24.9491,248.0085,-24.9412,247.9998,-24.9412,247.9998,-24.9491)"
        }
        args = argparse.Namespace(collection='alma_obscore')
        expected = {'pid': 'ALMA01059076_00_00_00',
                    'name': 'emb_10',
                    'dp_type': 'IMAGE',
                    'format': 'fits',
                    'locality': 'online',
                    'access_url': 'http://jvo.nao.ac.jp/skynode/do/download/alma/public/siav2/ALMA01059076_00_00_00',
                    'ra': 248.00413143,
                    'dec': -24.9451725,
                    'equinox': '2000.0',
                    'release_date': '2017-11-27',
                    'data_provider': 'ALMA',
                    'sky_footprint': 'POLYGON(248.0085,-24.9491,248.0085,-24.9412,247.9998,-24.9412,247.9998,-24.9491)',
                    'dataset_id': 'ALMA01059076_00_00_00',
                    'activity': None,
                    'collection': 'alma_obscore'}

        # act
        connector = ALMA_Obscore()
        actual = connector.translate(row=row, args=args)

        # assert contents ignoring order
        self.assertCountEqual(actual, expected)
