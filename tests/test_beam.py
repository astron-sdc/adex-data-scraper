import unittest

from scripts.beam import calc_beam


class TestBeam(unittest.TestCase):
    def test_calc_beam_no_beam_number_at_all(self):
        # arrange
        name = "abc"
        expected = 0

        # act
        actual = calc_beam(name)

        # assert
        self.assertEqual(actual, expected)

    def test_calc_beam_no_beam_number_but_partial_match_no_dot(self):
        # arrange
        name = "_BXXXXX"
        expected = 0

        # act
        actual = calc_beam(name)

        # assert
        self.assertEqual(actual, expected)

    def test_calc_beam_valid_beam_number(self):
        # arrange
        name = "_B099."
        expected = 99

        # act
        actual = calc_beam(name)

        # assert
        self.assertEqual(actual, expected)
