""" ADEX data scraper """

try:
    from importlib import metadata
except ImportError:  # for Python<3.8
    import importlib_metadata as metadata

try:
    __version__ = metadata.version("adex-data-scraper")
except metadata.PackageNotFoundError:
    # package is not installed
    __version__ = "unknown"
    pass
