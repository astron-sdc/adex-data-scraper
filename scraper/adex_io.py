import requests
import datetime

TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

class ADEX:
    ADEX_HEADER = {
        'content-type': "application/json",
        'cache-control': "no-cache"
    }

    def __init__(self, args):
        """
        Constructor.
        """
        self.args = args
        self.verbose = args.verbose
        self.adex_backend_host = args.adex_backend_host
        self.adex_resource = args.adex_resource
        self.header = self.ADEX_HEADER
        self.header['Authorization'] = f'Token {args.adex_token}'
        self._session = None

    def session(self):
        if self._session is None:
            self._session = requests.Session()
            self._session.headers.update(self.header)
        return self._session

    def verbose_print(self, info_str):
        """
        Print info string if verbose is enabled (default False)
        :param info_str: String to print
        """
        if self.verbose:
            timestamp = datetime.datetime.now().strftime(TIME_FORMAT)
            print(str(timestamp) + ' - ' + info_str)

    def _request(self, url, type, query_parameters=None, payload=None):
        #if not url.endswith('/'):
        #    url += '/'

        if type == 'POST':
            if isinstance(payload, str):
                response = self.session().request(type, url, data=payload, headers=self.header, params=query_parameters)
            else:
                response = self.session().request(type, url, json=payload, headers=self.header, params=query_parameters)
        else:
            response = self.session().request(type, url, headers=self.header)

        self.verbose_print(f"[{type} {response.url} ]")
        self.verbose_print(
            "Response: " + str(response.status_code) + ", " + str(response.reason) + ", " + str(response.text))

        return response

    def do_clear_primary_dps(self, collection=None):
        """
        delete primary dataproducts from ADEX backend
        :param collection: if given, only delete dataproducts for this collection. Otherwise, delete all.
        :return
        """
        if collection:
            url = self.adex_backend_host + 'clear-primary-dps' + '?collection=' + collection
        else:
            url = self.adex_backend_host + 'clear-primary-dps'

        try:
            response = self._request(url, 'GET')

        except Exception as error:
            try:
                raise Exception(response)
            except:
                raise Exception(error)

    def do_clear_ancillary_dps(self, collection=None):
        """
        delete ancillary dataproducts from ADEX backend
        :param collection: if given, only delete dataproducts for this collection. Otherwise, delete all.
        :return
        """
        if collection:
            url = self.adex_backend_host + 'clear-ancillary-dps' + '?collection=' + collection
        else:
            url = self.adex_backend_host + 'clear-ancillary-dps'

        try:
            response = self._request(url, 'GET')

        except Exception as error:
            try:
                raise Exception(response)
            except:
                raise Exception(error)


    def do_clear_activities(self, collection=None):
        """
        delete ancillary dataproducts from ADEX backend
        :param collection: if given, only delete dataproducts for this collection. Otherwise, delete all.
        :return
        """
        if collection:
            url = self.adex_backend_host + 'clear-activities' + '?collection=' + collection
        else:
            url = self.adex_backend_host + 'clear-activities'

        try:
            response = self._request(url, 'GET')

        except Exception as error:
            try:
                raise Exception(response)
            except:
                raise Exception(error)


    def check_clear_resources(self, args):
        """
        delete dataproducts from ADEX backend
        :param args: commandline parameters, used to either delete all records or only the collection
        :return
        """
        if args.simulate_post:
            return

        # clear the current cache (only the indicated resource)
        if args.clear_resource:
            print('delete all records from ' + args.adex_resource)

            if 'primary_dp' in args.adex_resource:
                self.do_clear_primary_dps()

            if 'ancillary_dp' in args.adex_resource:
                self.do_clear_ancillary_dps()

        # clear the current cache (only the indicated collection
        if args.clear_collection:
            if args.collection:
                print('delete all records from ' + args.adex_resource + ' for collection ' + args.collection)

                if 'primary_dp' in args.adex_resource:
                    self.do_clear_primary_dps(collection=args.collection)
                    self.do_clear_activities(collection=args.collection)

                if 'ancillary_dp' in args.adex_resource:
                    self.do_clear_ancillary_dps(collection=args.collection)

    def do_POST_json(self, adex_endpoint, payload):
        """
        POST a payload to a resource (table). This creates a new object (observation or dataproduct)
        This function replaces the old do_POST function that still needed to convert the json content in a very ugly
        :param resource: contains the resource, for example 'observations', 'dataproducts'
        :param payload: the contents of the object to create in json format
        """

        url = self.adex_backend_host + 'api/v1/' + adex_endpoint
        if not url.endswith('/'):
            url += '/'

        self.verbose_print((f'payload: {payload} url: {url}'))

        try:
            response = self._request(url, 'POST', payload=payload)
            if response.status_code > 300:
                raise Exception(response.text)

        except Exception as error:
            try:
                raise Exception(response.json())
            except:
                raise Exception(error)
