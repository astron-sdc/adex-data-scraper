import datetime


class DuplicateUnspecified():
    """
    This is the (Oracle) connector that reads a simple example from the LTA
    """

    def __init__(self):
        self.sql_select_statement = """
        SELECT unspec_dp.filename, COUNT(*) AS Occurrence
        FROM AWOPER.UnspecifiedDataProduct unspec_dp
        GROUP BY unspec_dp.filename HAVING COUNT(*)>1
        """

    def translate(self, row, args):
        """
        parse the specific row that comes from the postgres SQL query (sql_select_statement),
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the SQL query to a postgres service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: payload: the json payload for the post to the ADEX backend
        """
        payload = dict(
            name=row[0],
            count = row[1]
        )

        return payload


class SkyImages():
    """
    This is the (Oracle) connector that reads a simple example from the LTA
    """

    def __init__(self):
        self.sql_select_statement = """
        SELECT *
        FROM AWOPER.SkyImageDataProduct dp
        JOIN AWOPER.Pointing P ON P.OBJECT_ID = dp.OBSERVATIONPOINTING
        JOIN AWOPER.ProjectInformation PR ON PR.OBJECT_ID = dp.PROJECTINFORMATION
        """
    def translate(self, row, args):
        """
        parse the specific row that comes from the postgres SQL query (sql_select_statement),
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the SQL query to a postgres service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: payload: the json payload for the post to the ADEX backend
        """
        payload = dict(
            pid=str(row[15]),
            project_name = row[74],
            name=row[11],
            dp_type="skyimage",
            format=row[14], #this yields 'archive/tar', which is not a defined format by stakeholders

            locality="tape",
            #access_url=row['access_url'],
            ra=float(row[61]),
            dec=float(row[59]),
            equinox=str(row[60]),

            #timestamp=row[10],
            data_provider="ASTRON",

            dataset_id=row[11].split('_')[0],
            collection=args.collection,

            pipeline_url=row[23],

            direction_coordinate = str(row[32])
        )

        return payload