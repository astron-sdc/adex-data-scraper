
import cx_Oracle
import datetime

from scraper.adex_io import ADEX
from scraper.oracle.connectors import lta

def parse_database_url(url):
    """"
    parse a database url like: awtier0:<password>@db.lofar.target.rug.nl:1521
    and cut it up in convenient parts for the psycopg2.connect
    :param url: url that describes a database connection
    :return: user, password, host, database, db_port
    """
    # parse database url like: awtier0:<password>>@db.lofar.target.rug.nl:1521

    if "oracle://" in url:
        url = url.replace('oracle://','')

    userpass = url.split('@')[0]
    user = userpass.split(':')[0]
    password = userpass.split(':')[1]

    host_and_port = url.split('@')[1]
    host     = host_and_port.split(":")[0]
    db_port  = host_and_port.split(":")[1]

    return user, password, host, db_port

def run(args):
    """"
    run the scraper Oracle functionality with the parameters that come in through args
    :param args: the object with commandline parameters
    :return
    """

    print('oracle_scraper.run...')

    try:
        # init an ADEX instance
        adex = ADEX(args)
        adex.check_clear_resources(args)

        # instantiate connector for translation from oracle to ADEX
        connector_module,connector_name = args.connector.split('.')

        if connector_module.upper() == 'LTA':
            connector_class = getattr(lta, connector_name)
            connector = connector_class()

        # get the credentials and connection info from the data_host parameter
        user, password, host, db_port = parse_database_url(args.data_host)

        # if the oracle client library is given as a parameter, then initialize it
        if args.oracle_lib_dir:
            cx_Oracle.init_oracle_client(lib_dir=args.oracle_lib_dir)

        # connect to oracle database
        LTADSN = cx_Oracle.makedsn(host, db_port, service_name=host)
        source_connection = cx_Oracle.connect(
            dsn=LTADSN,
            user=user,
            password=password)

        source_cursor = source_connection.cursor()

        # read the data (oracle is fast enough to not use batches)
        print('fetching records from the oracle database...')

        source_cursor.execute(connector.sql_select_statement)
        rows = source_cursor.fetchall()
        count = len(rows)
        print(f"{count} records fetched from {args.data_host}")

        # iterate the results and store them in the adex_cache database
        records = []
        t1 = datetime.datetime.now()

        for row in rows:
            record = connector.translate(row, args)
            if not record:
                continue

            # construct a list of records
            records.append(record)

        # post to ADEX in batches
        t2 = datetime.datetime.now()
        print(f"{count} translated and ready to post {args.adex_backend_host} in {t2 - t1}")
        batch = []
        total_count = 0

        for record in records:
            batch.append(record)

            # when reaching the batch_size, post to ADEX backend
            if len(batch) == int(args.batch_size):
                total_count = total_count + len(batch)

                t1 = datetime.datetime.now()
                if not args.simulate_post:
                    adex.do_POST_json(args.adex_resource, batch)
                t2 = datetime.datetime.now()

                print(f"{len(batch)} ({total_count}) posted to {args.adex_backend_host} in {t2-t1}")
                batch.clear()

        # post the remainder
        t1 = datetime.datetime.now()
        if not args.simulate_post:
            adex.do_POST_json(args.adex_resource, batch)
        t2 = datetime.datetime.now()
        total_count = total_count + len(batch)
        print(f"{len(batch)} ({total_count}) posted to {args.adex_backend_host} in {t2 - t1}")


    except Exception as error:
        print(f"ERROR: {error}")
        return

