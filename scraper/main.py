import os, sys, argparse
from scraper.vo import vo_scraper
from scraper.postgres import postgres_scraper
from scraper.oracle import oracle_scraper
import scraper

def main():
    def get_arguments(parser):
        """
        Gets the arguments with which this application is called and returns
        the parsed arguments.
        If a argfile is give as argument, the arguments will be overrided
        The args.argfile need to be an absolute path!
        :param parser: the argument parser.
        :return: Returns the arguments.
        """
        args = parser.parse_args()
        if args.argfile:
            args_file = args.argfile
            if os.path.exists(args_file):
                parse_args_params = ['@' + args_file]
                # First add argument file
                # Now add command-line arguments to allow override of settings from file.
                for arg in sys.argv[1:]:  # Ignore first argument, since it is the path to the python script itself
                    parse_args_params.append(arg)
                print(parse_args_params)
                args = parser.parse_args(parse_args_params)
            else:
                raise (Exception("Can not find parameter file " + args_file))
        return args


    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
    parser.add_argument("--datasource",
                        default="vo",
                        help="where should the data be imported from? Options: vo, postgres")

    parser.add_argument("--data_host",
                        default="https://vo.astron.nl/tap/",
                        help="service/table, either VO or postgres. Examples: https://vo.astron.nl/tap/apertif_dr1.continuum_images, postgres:postgres@localhost:5432/alta")
    parser.add_argument("--oracle_lib_dir",
                        default="D:\oracle\instantclient_21_10",
                        help="Directory where oracle client libraries are installed (only needed when using oracle connectors and the oracle libraries are not available on the path")

    parser.add_argument("--connector",
                        default="lotss_dr2_mosaics_connector",
                        help="Connector class containing the translation scheme from vo to adex")

    parser.add_argument("--limit",
                        default="0",
                        help="max records to fetch from VO, for dev/test purposes")

    parser.add_argument("--batch_size",
                        default="1000",
                        help="number of records to post as a batch to ADEX")

    parser.add_argument("--adex_backend_host",
                        nargs="?",
                        default='http://localhost:8000/adex_backend/',
                        help="location of the adex-backend-django application")

    parser.add_argument("--adex_resource",
                        default="primary_dp/create",
                        help="resource/table to update, options are: primary_dp/create, ancillary_dp/create, activity/create")

    parser.add_argument("--collection",
                        default=None,
                        help="can be used as filter in ADEX backend and ADEX frontend")

    parser.add_argument("--clear_resource",
                        default=False,
                        help="Delete all the data from the adex_resource",
                        action="store_true")

    parser.add_argument("--clear_collection",
                        default=False,
                        help="Delete all the data for this collection from the adex_resource, works in concert with the '--collection' parameter.",
                        action="store_true")

    parser.add_argument("--simulate_post",
                        default=False,
                        help="If true, then no data is posted to ADEX",
                        action="store_true")
    # token is generated manually in the admin app
    parser.add_argument("--adex_token", default="9519e433ba37487f1a18121dfb1957d992fbb790", help="Token to login")

    parser.add_argument("-v", "--verbose",
                        default=False,
                        help="More information about atdb_services at run time.",
                        action="store_true")

    # All parameters in a file
    parser.add_argument('--argfile',
                        nargs='?',
                        type=str,
                        help='Ascii file with arguments (overrides all other arguments')

    args = get_arguments(parser)

    print(f"--- adex-data-scraper (version 23 aug 2024) ---")
    print(args)

    if args.datasource.upper() == 'VO':
        vo_scraper.run(args)

    if args.datasource.upper() == 'POSTGRES':
        postgres_scraper.run(args)

    if args.datasource.upper() == 'ORACLE':
        oracle_scraper.run(args)

if __name__ == '__main__':
    main()
