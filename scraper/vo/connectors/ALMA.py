
class Obscore():

    def translate(self, row, args):
        """
        parse the specific row that comes from the VO adql query,
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the ADQL query to a VO service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: ADEX record as json structure
        """
        payload = dict(
            pid=row['data_id'],
            name=row['target_name'],
            dp_type=row['dataproduct_type'],
            format="fits",
            locality="online",
            access_url=row['access_url'],
            ra=float(row['s_ra']),
            dec=float(row['s_dec']),
            equinox="2000.0",

            release_date=row['obs_release_date'],
            data_provider="ALMA",

            sky_footprint=row['s_region'],

            dataset_id=str(row['data_id']),
            activity=None,
            collection = args.collection,
        )

        return payload

