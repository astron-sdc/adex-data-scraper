import datetime

class ContinuumImagesConnector():

    def translate(self, row, args):
        """
        parse the specific row that comes from the VO adql query,
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the ADQL query to a VO service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: ADEX record as json structure
        """
        freq_min = float(row['freqmin'])
        freq_max = float(row['freqmax'])
        freq_avg = (freq_max + freq_min) / 2

        payload = dict(
            pid=row['pub_did'],
            name=row['imagetitle'],
            dp_type="science-skymap",
            format="fits",
            locality="online",
            access_url=row['accref'],
            ra=float(row['centeralpha']),
            dec=float(row['centerdelta']),
            equinox="2000.0",

            exposure_time=None,
            central_frequency=freq_avg,
            frequency_resolution=None,
            time_resolution=None,
            bandwidth=None,

            release_date=datetime.datetime.utcnow().isoformat(),
            data_provider="ASTRON",

            PSF_size=None,
            sky_footprint=str(row['coverage']),

            dataset_id=str(row['obsid']),
            collection=args.collection,
            pipeline_url='apercal_pipeline',
            pipeline_version="2.5",
        )

        # useful settings, do something with it?
        #dateobs = row['dateobs']
        #mime = row['mime']
        #bandpassid = row['bandpassid'],

        return payload


