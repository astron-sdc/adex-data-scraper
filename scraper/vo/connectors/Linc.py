import datetime

class LincSkymap():

    def translate(self, row, args):
        """
        parse the specific row that comes from the VO adql query,
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the ADQL query to a VO service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: payload in json format
        """

        # note that frequencies are in Hz in this case, while in other tables it is in Mhz,
        # hence the division by 1000000 here. But which units are expected?
        # TODO: This question and more are recorded in this ticket: https://support.astron.nl/jira/browse/SDC-926
        freq_min = float(row['f_min'] / 1000000)
        freq_max = float(row['f_max'] / 1000000)
        freq_avg = (freq_max + freq_min) / 2

        duration = float(row['t_end']) - float(row['t_start'])
        #sampling_time = duration / time_samples

        # do we need this is to link activities to dataproducts? or can we use row['activity'], which contains the SAS_ID.
        #pipeline_run_id = row['pipeline_run_id']

        payload = dict(
            pid=row['imageTitle'],
            name=row['imageTitle'],
            dp_type="qa-skymap",
            format=row['mime'],
            locality="online",
            access_url=row['accref'],
            ra=float(row['centerAlpha']),
            dec=float(row['centerDelta']),
            equinox=str(row['wcs_equinox']),
            exposure_time=duration,
            central_frequency=freq_avg,

            frequency_resolution=None,
            time_resolution=None,
            bandwidth=None,

            release_date=None,
            data_provider="ASTRON",

            PSF_size=None,
            #sky_footprint=str(row['s_region']),
            sky_footprint=None,
            # is this enough? or should the dataproducts be linked to the pipeline through 'pipeline_run_id' instead?

            dataset_id=str(row['activity']),
            collection=args.collection,

            pipeline_url=row['pipeline_access_url'],
            pipeline_version=row['pipeline_version'],
        )

        return payload


class LincCalibratedVisibilities():

    def translate(self, row, args):
        """
        parse the specific row that comes from the VO adql query,
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the ADQL query to a VO service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: payload in json format
        """
        freq_min = float(row['f_min'])
        freq_max = float(row['f_max'])
        freq_avg = (freq_max + freq_min) / 2

        payload = dict(
            pid=row['id'],
            name=row['id'],
            dp_type="die-calibrated-visibilities",
            format=row['access_format'], #this yields 'archive/tar', which is not a defined format by stakeholders
            #format="other",
            locality="tape",
            access_url=row['access_url'],
            ra=float(row['ra']),
            dec=float(row['dec']),
            equinox=str(row['equinox']),

            exposure_time=str(row['t_exptime']),
            central_frequency=freq_avg,
            frequency_resolution=None,
            time_resolution=None,
            bandwidth=float(row['freq_bandwidth']),

            release_date=None,
            data_provider="ASTRON",

            PSF_size=None,
            sky_footprint=str(row['s_region']),

            dataset_id=str(row['activity']),
            collection=args.collection,

            pipeline_url=row['pipeline_access_url'],
            pipeline_version = row['pipeline_version'],

        )

        # are these parameters that we can create the Acitivies with?

        #related_products = row['related_products'],
        return payload


class LincSecondary():

    def translate(self, row, args):
        """"
        parse the specific row that comes from the VO adql query,
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the ADQL query to a VO service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: payload in json format
        """

        # check for these types from VO: inputs, inspection-plot, execution-logs, calibration-file

        # translate VO speak to ADEX speak
        dp_type = row['type']
        if dp_type == 'inspection-plot':
            dp_type = 'diagnostic-plot'

        if dp_type == 'calibration-file':
            dp_type = 'calibration-solution'

        if dp_type == 'inputs':
            dp_type = 'pipeline-config'

        if dp_type == 'execution-log':
            dp_type = 'execution-log'

        payload = dict(
            pid=row['access_url'],
            name=row['access_url'],
            dp_type=dp_type,
            format="other",
            locality="online",
            access_url=row['access_url'],

            dataset_id=str(row['activity']),
            collection = args.collection
        )

        return payload

