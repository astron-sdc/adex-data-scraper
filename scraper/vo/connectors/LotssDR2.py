import datetime
class LotssDR2Mosaics():

    def translate(self, row, args):
        """
        parse the specific row that comes from the VO adql query,
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the ADQL query to a VO service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: ADEX record as json structure
        """
        payload = dict(
            pid=row['data_pid'],
            name=row['imagetitle'],
            dp_type="skymap",
            format="other",
            locality="online",
            access_url=row['accref'],
            ra=float(row['centeralpha']),
            dec=float(row['centerdelta']),
            equinox="2000.0",

            exposure_time=None,
            central_frequency=None,
            frequency_resolution=None,
            time_resolution=None,
            bandwidth=None,

            release_date=datetime.datetime.utcnow().isoformat(),
            data_provider="ASTRON",

            PSF_size=None,
            sky_footprint=None,

            dataset_id=str(row['lofar_obsids'][0]),
            activity=None,
            collection = args.collection,
        )

        # useful settings, do something with it?
        #collection = 'lotss_dr2_mosaics'
        #dataset_ids = row['lofar_obsids'],
        #dateobs = row['dateobs']
        #mime = row['mime']
        #bandpassid = row['bandpassid'],

        return payload

