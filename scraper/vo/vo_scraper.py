
import pyvo

import datetime
import logging
logger = logging.getLogger('vo_scraper')

from scraper.adex_io import ADEX
from scraper.vo.connectors import Apertif_DR1, LotssDR2, Linc, ALMA

def run(args):
    """
    run the scraper VO functionality with the parameters that come in through args
    :param args: the object with commandline parameters
    :return
    """

    print('vo_scraper.run...')
    logger.info("vo_scraper.run...")


    try:
        adex = ADEX(args)
        adex.check_clear_resources(args)

        vo_service, vo_table = args.data_host.rsplit('/', 1)

        # instantiate connector for translation from VO to ADEX
        connector_module,connector_name = args.connector.split('.')

        if connector_module.upper() == 'APERTIF_DR1':
            connector_class = getattr(Apertif_DR1, connector_name)
            connector = connector_class()

        if connector_module.upper() == 'LOTSS_DR2':
            connector_class = getattr(LotssDR2, connector_name)
            connector = connector_class()

        if connector_module.upper() == 'LINC':
            connector_class = getattr(Linc, connector_name)
            connector = connector_class()

        if connector_module.upper() == 'ALMA':
            connector_class = getattr(ALMA, connector_name)
            connector = connector_class()

        # construct the ADQL query
        #select_fields = "centeralpha,centerdelta,imagetitle,accref"
        select_fields = "*"

        # iterate through the VO service in batches
        t0 = datetime.datetime.now()
        offset = 0
        total_count = 0

        while (True):

            # construct the ADQL query
            query = "SELECT TOP " + str(args.batch_size) + " " + select_fields +" FROM " + vo_table + " OFFSET "+ str(offset)

            # query the VO service
            service = pyvo.dal.TAPService(vo_service)
            resultset = service.search(query)

            count = len(resultset)
            if count == 0:
                break

            print(f"{count} records fetched from {vo_service}")

            # iterate the results and store them in the adex_cache database
            records = []
            t1 = datetime.datetime.now()

            for row in resultset:

                record = connector.translate(row, args)
                if not record:
                    continue

                # construct a list of records
                records.append(record)

            # post a batch of records
            adex.do_POST_json(args.adex_resource, records)
            t2 = datetime.datetime.now()

            print(f"{count} posted to {args.adex_backend_host} in {t2-t1}")

            # next iteration?
            total_count = total_count + count
            if (int(args.limit)>0) & (total_count >= int(args.limit)):
                break

            offset = offset + int(args.batch_size)

        t3 = datetime.datetime.now()
        print(f"{total_count} records in total in  {t3 - t0}")

    except UnboundLocalError as error:
        print(f"ERROR: {error}. Did you import and hook up the {connector_module} connector in vo_scraper.py?")
        return
    except Exception as error:
        print(f"ERROR: {error}.")
        return

