
import psycopg2
from psycopg2 import Error

import datetime

from scraper.adex_io import ADEX
from scraper.postgres.connectors import apertif

def parse_database_url(url):
    """"
    parse a database url like: postgres:postgres@localhost:5432/alta
    and cut it up in convenient parts for the psycopg2.connect
    :param url: url that describes a database connection
    :return: user, password, host, database, db_port
    """
    # parse database url like: postgres:postgres@localhost:5432/alta
    # get user, password
    if "postgresql://" in url:
        url = url.replace('postgresql://','')

    userpass = url.split('@')[0]
    user = userpass.split(':')[0]
    password = userpass.split(':')[1]

    hostdatabase = url.split('@')[1]

    database = hostdatabase.split('/')[1]
    hostport = hostdatabase.split('/')[0]
    host     = hostport.split(":")[0]
    db_port  = hostport.split(":")[1]

    return user, password, host, database, db_port

def run(args):
    """"
    run the scraper Postgres functionality with the parameters that come in through args
    :param args: the object with commandline parameters
    :return
    """

    print('postgres_scraper.run...')

    try:
        adex = ADEX(args)
        adex.check_clear_resources(args)

        # instantiate connector for translation from postgres to ADEX

        connector_module,connector_name = args.connector.split('.')

        if connector_module.upper() == 'APERTIF':
            connector_class = getattr(apertif, connector_name)
            connector = connector_class()

        # open the source postgres database
        user, password, host, database, db_port = parse_database_url(args.data_host)
        source_connection = psycopg2.connect(
            database=database,
            user=user,
            password=password,
            host=host,
            port=db_port
        )
        source_cursor = source_connection.cursor()

        # read the data (postgres is fast enough to not use batches)
        print('fetching records from the postgres database...')
        source_cursor.execute(connector.sql_select_statement)
        rows = source_cursor.fetchall()
        count = len(rows)
        print(f"{count} records fetched from {args.data_host}")

        # iterate the results and store them in the adex_cache database
        records = []
        t1 = datetime.datetime.now()

        for row in rows:
            record = connector.translate(row, args)
            if not record:
                continue

            # construct a list of records
            records.append(record)

        # post to ADEX in batches
        t2 = datetime.datetime.now()
        print(f"{count} translated and ready to post {args.adex_backend_host} in {t2 - t1}")
        batch = []
        total_count = 0

        for record in records:
            batch.append(record)

            # when reaching the batch_size, post to ADEX backend
            if len(batch) == int(args.batch_size):
                total_count = total_count + len(batch)

                t1 = datetime.datetime.now()
                if not args.simulate_post:
                    adex.do_POST_json(args.adex_resource, batch)
                t2 = datetime.datetime.now()

                print(f"{len(batch)} ({total_count}) posted to {args.adex_backend_host} in {t2-t1}")
                batch.clear()

        # post the remainder
        t1 = datetime.datetime.now()
        if not args.simulate_post:
            adex.do_POST_json(args.adex_resource, batch)
        t2 = datetime.datetime.now()
        total_count = total_count + len(batch)
        print(f"{len(batch)} ({total_count}) posted to {args.adex_backend_host} in {t2 - t1}")


    except Exception as error:
        print(f"ERROR: {error}")
        return

