import datetime


class Inspectionplots():
    """
    This is the (Postgres) connector to translate APERTIF inspectionplots to ancillery dataproducts in ADEX
    """

    def __init__(self):
        self.sql_select_statement = """
        SELECT 
        "name" as name,
        "PID" as pid,
        "storageRef" as access_url, 
        "dataProductType" as dt, 
        "dataProductSubType" as dst, 
        "datasetID" as observation 

        FROM api_dataproduct as table1
        INNER JOIN api_dataentity as table2
        INNER JOIN api_entity as table3
        ON table3.altaobject_ptr_id = table2.entity_ptr_id
        ON table1.dataentity_ptr_id = table2.entity_ptr_id

        WHERE "dataProductType" = 'inspectionPlot';
        """

    def translate(self, row, args):
        """
        parse the specific row that comes from the postgres SQL query (sql_select_statement),
        and translate it into the standard json payload for posting to the ADEX backend REST API

        :param row: the results from the SQL query to a postgres service
        :param args: the commandline arguments, but only args.collection is currently used
        :return: payload: the json payload for the post to the ADEX backend
        """
        payload = dict(
            pid=row[1],
            name=row[0],
            dp_type="diagnostic-plots",
            format="png",
            locality="online",
            access_url=row[2],

            dataset_id=str(row[5]),
            collection=args.collection
        )

        return payload
